const CACHE_NAME = 'site-cache-v1';

self.addEventListener('install', (evt) => {
  evt.waitUntil(
    caches.open(CACHE_NAME).then((cache) => {
      return cache.addAll([
        '/',
        '/assets/css/mystyle.css',
        '/assets/css/style.css',
        '/assets/css/beauty.css',
        '/assets/js/index2.js',
        '/assets/js/mapbox.js',
        '/assets/js/add2numbers.js',
        '/project1/add2numbers.html',
        '/project2/index.html',
        '/assets/font/feast-of-flesh-bb/fofbb_reg.ttf',
        '/assets/images/DSC_0144.JPG'
      ]).then( (resp) => {
        // console.log("resp berhasil kayaknya")
      }).catch((err) => {
        // console.log(err);
      })
    })
  )
});

self.addEventListener('fetch', (evt) => {
  evt.respondWith(
    caches.match(evt.request)
      .then((res) => {
        if(res) return res;

        // console.log(evt.request)

        return fetch(evt.request);
      })  
  )
})