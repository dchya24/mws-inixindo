const position = [-6.221028, 106.791434]
const map = L.map('map').setView(position, 16);
L.tileLayer(
  'https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}',
  {
    attribution: 'Map data &copy; Cahya Dinar',
    maxZoom: 20,
    id : 'mapbox.dark',
    accessToken : 'pk.eyJ1IjoiZGNhaHlhMjQiLCJhIjoiY2psbWIzN2Z5MTY5NzN2bXgzdjYxY3I2ciJ9.44XU5Bcp0OYsrYZ_FEbPCg'
}).addTo(map);


const URL = "/assets/data.json";
fetch(URL).then((resp) => {
  return resp.json()
}).then((res) => {
  localStorage.setItem('data', JSON.stringify(res))
});
// (async () => {
//   try {
//     let resp1 = await(fetch(URL));
//     let resp2 = await resp1.json();
//     console.log(resp2)
//     localStorage.setItem('data', JSON.stringify(resp2))
//   }catch(err){
//     console.log(err);
//   }
// })

const data = JSON.parse(localStorage.getItem('data'));
for (var p of data) {
  var marker= L.marker(p.lokasi).addTo(map)
  .bindPopup(p.nama);
  marker.on('click', showLocation);
}

function showLocation(e) {
  let ix = findLocation(e.latlng.lat,e.latlng.lng);
  addImage(data[ix].gambar);
  addComment(data[ix].review);
}

const findLocation = (x,y) => {
  var i;
  for(i =0; i < data.length; i++){
    if(data[i].lokasi[0] == x && data[i].lokasi[1] == y){
      return i;
    }
  }
}

let imageDiv = document.querySelector(".gambar");
let reviewDiv = document.querySelector('.review');

const addImage = (data) => {
  var i;
  imageDiv.innerHTML = ""

  for(i=0; i < data.length; i++) {
    imageDiv.innerHTML += `<img src="/assets/images/${data[i]}" >`
  }
}

const addComment = (data) => {
  var i;
  reviewDiv.innerHTML = ""

  for(i=0; i < data.length; i++) {
    reviewDiv.innerHTML += `<div class='comment'>
    <div class='name'> ${data[i].nama} </div>
    <div class='comment-review'> ${data[i].review} </div> </div>`
  }
}