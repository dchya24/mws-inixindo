if (!('serviceWorker' in navigator)) {
  console.log('Browser tidak mendukung Service worker');
  }
  else {
    navigator.serviceWorker.register('/service-worker.js', {
      scope: '/'
    })
    .then((rgs) => {
      console.log("ServiceWorer registration successful with scope: ", rgs.scope)
    }).catch((err) => {
      console.log("ServiceWorker registration failed:", err)
    });
}


// const addBtn = document.getElementById('btnPwa');
// let deferredPrompt;

// window.addEventListener('beforeinstallprompt', (e) => {
//  e.preventDefault();
//  deferredPrompt = e;
// });

// addBtn.addEventListener('click', (e) => {
//   deferredPrompt.prompt();
//   deferredPrompt.userChoice.then((choiceResult) => {
//     if(choiceResult.outcome === 'accepted') console.log('User accepted the a2hs prompt')
//     else console.log('user dismissed a2hs prompt');
//     defferedPrompt = null;
//   })
// })